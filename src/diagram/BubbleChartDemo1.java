/*---------------------------------------------------
 * Hochschule f�r Technik Stuttgart
 * Fachbereich Vermessung, Informatik und Mathematik
 * Schellingstr. 24
 * D-70174 Stuttgart
 *
 * Volker Coors, 11.9.2015
 * GeoVisualisierung
 * IL3, WS 2015/16 
 * 
 * ------------------------------------------------*/

package diagram;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import data.*;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.BubbleChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;



public class BubbleChartDemo1 extends Application {

	/**
	 * Erzeugt einen Datensatz mit n Elementen
	 * 
	 * @param daten
	 * @return CategoryDataset
	 */
	private ObservableList<XYChart.Data<Number, Number>> createRandomXYDataSet(int n) {

		ObservableList<XYChart.Data<Number, Number>> list = 
				FXCollections.observableArrayList();
		
		for (int i = 0; i < n; i++) {
			
			list.add(new XYChart.Data<Number, Number>(
					Math.random()+0.1, // x-Wert 
					Math.random()+0.1, // y-Wert
					Math.random()/10.0 // radius
					));
		}

		return list;
	}
	
    @Override public void start(Stage stage) {

    	// Daten, die im Diagramm dargestellt werden sollen
    	
    	// Erstellen und Beschriften des Diagramms
        final NumberAxis xAxis = new NumberAxis(0, 1.2, 0.1);
        final NumberAxis yAxis = new NumberAxis(0, 1.2, 0.1);
        
        final BubbleChart<Number,Number> bc = 
            new BubbleChart<Number,Number>(xAxis,yAxis);
        bc.setTitle("Random data set");
        xAxis.setLabel("x");       
        yAxis.setLabel("y");
        
        // Zuordnung der Daten zum Diagramm
       	ObservableList<XYChart.Data<Number, Number>> barChartData;
    	barChartData = createRandomXYDataSet(10);
        XYChart.Series<Number,Number> series1 = 
        		new XYChart.Series<Number,Number>(barChartData);
        series1.setName("Reihe 1");
        bc.getData().add(series1);

        XYChart.Series<Number,Number> series2 = 
        		new XYChart.Series<Number,Number>(createRandomXYDataSet(10));
        series2.setName("Reihe 2");
        bc.getData().add(series2);

        // Rendern des Diagramms
        Scene scene  = new Scene(bc,800,800);
        stage.setTitle("GVI Beispiel: Blasendiagramm");
        stage.setScene(scene);
        stage.show();
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
		
	}
}
